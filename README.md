# Ejemplo MS con mongo 

## Getting Started  

Estas instrucciones permitirán obtener una copia del proyecto funcional en tu máquina.

### Prerequisites  

Para correr este proyecto necesitas tener instalado lo siguiente:  

```  
JDK 1.8 o superior  
Maven 3.5.0 o superior  
Docker o MongoDB
```

## Built With

* [Maven](http://maven.apache.org/)


## MongoDB

Para correr un contenedor de MongoDB con docker ejecutar el comando.

```bash
docker run -d -p 27017:27017 --name test-mongo \
    -e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
    -e MONGO_INITDB_ROOT_PASSWORD=secret \
    mongo
```

## Generate jar

Para generar el archivo FiinlabSMS.jar ejecutar el comando.

```bash
mvn clean package
```

## Run jar

Ejecutar el comando.
```bash
java -jar target/*.jar
```

## Run with mvn

Ejecutar el comando.

```bash
mvn spring-boot:run
```

## Acknowledgments  

* A [Google](https://www.google.com.mx/) por su eterna sabiduría.  
* A [Stack Overflow](https://stackoverflow.com/) por tener la respuesta a las preguntas de la vida.  
