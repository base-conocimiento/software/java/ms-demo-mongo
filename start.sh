#!/bin/bash

mongo_create() {
  docker run -d -p 27017:27017 --name test-mongo \
    -e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
    -e MONGO_INITDB_ROOT_PASSWORD=secret \
    mongo
}

mvn spring-boot:run