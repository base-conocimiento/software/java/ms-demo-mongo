package com.mongo.demo.controller;

import com.mongo.demo.dto.ClienteRequest;
import com.mongo.demo.dto.ClienteResponse;
import com.mongo.demo.dto.ClienteUpdateRequest;
import com.mongo.demo.dto.ClientesResponse;
import com.mongo.demo.repository.ClienteDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.math.BigInteger;

@Api(tags = "clientes")
@RestController
@RequestMapping("/cliente")
public class ClienteController {
    private final ClienteDao clienteDao;

    public ClienteController(ClienteDao clienteDao) {
        this.clienteDao = clienteDao;
    }

    @PostMapping("")
    @ApiOperation(value = "${ClienteController.create}")
    public ClienteResponse create(@Valid @RequestBody ClienteRequest clienteRequest) {
        return clienteDao.createClient(clienteRequest);
    }

    @GetMapping("")
    @ApiOperation(value = "${ClienteController.getAll}")
    public ClientesResponse getAll() {
        return clienteDao.getAllClient();
    }

    @GetMapping("/{idCliente}")
    @ApiOperation(value = "${ClienteController.getById}")
    public ClienteResponse getById(@PathVariable BigInteger idCliente) {
        return clienteDao.getClietById(idCliente);
    }

    @PutMapping("/{idCliente}")
    @ApiOperation(value = "${ClienteController.editClient}")
    public ClienteResponse editClient(@PathVariable BigInteger idCliente, @Valid @RequestBody ClienteUpdateRequest clienteUpdateRequest) {
        return clienteDao.editClient(idCliente, clienteUpdateRequest);
    }
}
