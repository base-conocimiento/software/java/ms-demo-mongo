package com.mongo.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.math.BigInteger;
import java.util.Date;

@Data
@Document
@Table(name = "Cliente", uniqueConstraints = @UniqueConstraint(columnNames = "correoElectronico"))
public class ClienteModel {
    @Id
    private BigInteger id;

    @Indexed(unique = true)
    private String nombreUsuario;

    @JsonIgnore
    private String password;

    private String nombre;

    private String apellidos;

    @Indexed(unique = true)
    private String correoElectronico;

    private Integer edad;

    private Float estatura;

    private Float peso;

    private Float imc;

    private Float geb;

    private Float eta;

    private Date fechaCreacion;

    private Date fechaActualizacion;
}
