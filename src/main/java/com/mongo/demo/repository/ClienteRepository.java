package com.mongo.demo.repository;

import com.mongo.demo.models.ClienteModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.math.BigInteger;

@Repository
public interface ClienteRepository extends MongoRepository<ClienteModel, BigInteger> {
}
