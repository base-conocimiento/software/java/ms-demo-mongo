package com.mongo.demo.repository;

import com.mongo.demo.dto.ClienteRequest;
import com.mongo.demo.dto.ClienteResponse;
import com.mongo.demo.dto.ClienteUpdateRequest;
import com.mongo.demo.dto.ClientesResponse;
import com.mongo.demo.models.ClienteModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
public class ClienteDao {
    private static final String TODO_OK = "Todo Ok";
    private static final String NO_CLIENTE = "No existe cliente";

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ClientesResponse getAllClient() {
        ClientesResponse clientesResponse = new ClientesResponse();
        clientesResponse.setClientes(mongoTemplate.findAll(ClienteModel.class));
        if (clientesResponse.getClientes().isEmpty()) {
            clientesResponse.setCveError(-1);
            clientesResponse.setCveMensaje(NO_CLIENTE);
            return clientesResponse;
        }
        clientesResponse.setCveError(0);
        clientesResponse.setCveMensaje(TODO_OK);
        return clientesResponse;
    }

    public ClienteResponse getClietById(BigInteger clientId) {
        Query query = new Query();
        query.addCriteria(where("id").is(clientId));
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setCliente(mongoTemplate.findOne(query, ClienteModel.class));
        if (clienteResponse.getCliente() == null) {
            clienteResponse.setCveError(-1);
            clienteResponse.setCveMensaje(NO_CLIENTE);
            return clienteResponse;
        }
        clienteResponse.setCveError(0);
        clienteResponse.setCveMensaje(TODO_OK);
        return clienteResponse;
    }

    public ClienteResponse createClient(ClienteRequest clienteRequest) {
        ClienteModel clienteModel = new ClienteModel();
        clienteModel.setNombre(clienteRequest.getNombre());
        clienteModel.setApellidos(clienteRequest.getApellidos());
        clienteModel.setCorreoElectronico(clienteRequest.getCorreoElectronico());
        clienteModel.setPassword(passwordEncoder.encode(clienteRequest.getPassword()));
        clienteModel.setNombreUsuario(clienteRequest.getNombreUsuario());

        Query orQuery = new Query();
        Criteria orCriteria = new Criteria();
        List<Criteria> orExpression = new ArrayList<>();
        orExpression.add(new Criteria().and("nombreUsuario").is(clienteRequest.getNombreUsuario()));
        orExpression.add(new Criteria().and("correoElectronico").is(clienteRequest.getCorreoElectronico()));
        orQuery.addCriteria(orCriteria.orOperator(orExpression.toArray(new Criteria[orExpression.size()])));
        List<ClienteModel> clienteModels = mongoTemplate.find(orQuery, ClienteModel.class);

        ClienteResponse clienteResponse = new ClienteResponse();
        if (!clienteModels.isEmpty()) {
            clienteResponse.setCliente(clienteModels.get(0));
            clienteResponse.setCveError(-1);
            clienteResponse.setCveMensaje("Ya existe cliente");
            return clienteResponse;
        }

        clienteModel.setFechaCreacion(new Date());
        clienteModel.setFechaActualizacion(new Date());
        clienteResponse.setCliente(clienteRepository.save(clienteModel));
        clienteResponse.setCveError(0);
        clienteResponse.setCveMensaje(TODO_OK);
        return clienteResponse;
    }

    public ClienteResponse editClient(BigInteger idCliente, ClienteUpdateRequest clienteUpdateRequest) {
        ClienteResponse clienteResponse = this.getClietById(idCliente);
        boolean update = false;
        if (clienteResponse.getCliente() == null) {
            clienteResponse.setCveError(-1);
            clienteResponse.setCveMensaje(NO_CLIENTE);
            return clienteResponse;
        }
        if (clienteUpdateRequest.getEdad() != null) {
            clienteResponse.getCliente().setEdad(clienteUpdateRequest.getEdad());
            update = true;
        }
        if (clienteUpdateRequest.getEstatura() != null) {
            clienteResponse.getCliente().setEstatura(clienteUpdateRequest.getEstatura());
            update = true;
        }
        if (clienteUpdateRequest.getPeso() != null) {
            clienteResponse.getCliente().setPeso(clienteUpdateRequest.getPeso());
            update = true;
        }
        if (clienteUpdateRequest.getGeb() != null) {
            clienteResponse.getCliente().setGeb(clienteUpdateRequest.getGeb());
            update = true;
        }
        if (update) {
            clienteResponse.getCliente().setFechaActualizacion(new Date());
            clienteResponse.setCliente(clienteRepository.save(clienteResponse.getCliente()));
            clienteResponse.setCveError(0);
            clienteResponse.setCveMensaje(TODO_OK);
            return clienteResponse;
        }
        clienteResponse.setCveError(-1);
        clienteResponse.setCveMensaje(NO_CLIENTE);
        return clienteResponse;
    }
}
