package com.mongo.demo.dto;

import com.mongo.demo.models.ClienteModel;
import lombok.Data;
import java.util.List;

@Data
public class ClientesResponse extends GenericResponse {
    List<ClienteModel> clientes;
}
