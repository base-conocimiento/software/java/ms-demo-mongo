package com.mongo.demo.dto;

import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ClienteRequest {
    @NotNull
    @NotBlank
    private String nombre;

    @NotNull
    @NotBlank
    private String apellidos;

    @NotNull
    @NotBlank
    private String nombreUsuario;

    @NotNull
    @NotBlank
    @Email(message = "Correo incorrecto.")
    private String correoElectronico;

    @NotNull
    @NotBlank
    private String password;
}
