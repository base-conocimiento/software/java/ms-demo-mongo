package com.mongo.demo.dto;

import lombok.Data;

@Data
public class ClienteUpdateRequest {
    private Integer edad;
    private Float estatura;
    private Float peso;
    private Float geb;
}
