package com.mongo.demo.dto;

import lombok.Data;
import javax.persistence.Transient;

@Data
public class GenericResponse {
    @Transient
    private int cveError;

    @Transient
    private String cveMensaje;
}
