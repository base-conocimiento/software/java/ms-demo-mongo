package com.mongo.demo.dto;

import com.mongo.demo.models.ClienteModel;
import lombok.Data;

@Data
public class ClienteResponse extends GenericResponse {
    ClienteModel cliente;
}
